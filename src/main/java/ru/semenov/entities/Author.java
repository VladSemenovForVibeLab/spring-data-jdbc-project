package ru.semenov.entities;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.MappedCollection;
import org.springframework.data.relational.core.mapping.Table;

import java.io.Serializable;
import java.util.Set;

@Table("authors")
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@Builder
@ToString
@EqualsAndHashCode
public class Author implements Serializable,Comparable<Author> {
    @Id
    private String id;
    private String name;
    @MappedCollection(idColumn = "author_id")
    private Set<Book> books;

    @Override
    public int compareTo(Author o) {
        return this.name.compareTo(o.name);
    }
}
