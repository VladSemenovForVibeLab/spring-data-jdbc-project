package ru.semenov.entities;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceCreator;
import org.springframework.data.relational.core.mapping.Table;

import java.io.Serializable;

@Table("categories")
@Getter
@Setter
@EqualsAndHashCode
@ToString
@Builder
public class Category implements Serializable,Comparable<Category> {
    @Id
    private String id;
    private String title;
    private Category (){}

    @PersistenceCreator
    public Category(String id, String title) {
        this.id = id;
        this.title = title;
    }

    @Override
    public int compareTo(Category o) {
        return this.title.compareTo(o.title);
    }
}
