package ru.semenov.entities;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.MappedCollection;
import org.springframework.data.relational.core.mapping.Table;

import java.io.Serializable;

@ToString
@EqualsAndHashCode
@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@Table("books")
public class Book implements Serializable,Comparable<Book> {
    @Id
    private String id;
    private String title;
    @MappedCollection(idColumn ="book_id")
    private BookDetails bookDetails;
    private String authorId;
    @Override
    public int compareTo(Book o) {
        return this.title.compareTo(o.title);
    }
}
