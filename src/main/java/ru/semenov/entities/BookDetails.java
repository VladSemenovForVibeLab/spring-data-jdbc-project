package ru.semenov.entities;

import lombok.*;
import org.springframework.data.relational.core.mapping.Table;

import java.io.Serializable;

@ToString
@Builder
@Getter
@Setter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@Table("books_details")
public class BookDetails implements Serializable,Comparable<BookDetails> {
    private String id;
    private String bookId;
    private String description;

    @Override
    public int compareTo(BookDetails o) {
        return this.description.compareTo(o.description);
    }
}
