package ru.semenov.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.semenov.dto.AuthorDTO;
import ru.semenov.services.IAuthorsService;

@RestController
@RequestMapping("/api/v1/authors")
public class AuthorsController {
    private final IAuthorsService authorsService;

    @Autowired
    public AuthorsController(IAuthorsService authorsService) {
        this.authorsService = authorsService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<AuthorDTO> findById(@PathVariable String id){
        return new ResponseEntity<AuthorDTO>(authorsService.findById(id), HttpStatus.FOUND);
    }
}
