package ru.semenov.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.service.annotation.GetExchange;
import ru.semenov.dto.BookDTO;
import ru.semenov.dto.DetailedBookDTO;
import ru.semenov.services.IBooksService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/books")
public class BooksController {
    private final IBooksService booksService;

    @Autowired
    public BooksController(IBooksService booksService) {
        this.booksService = booksService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<BookDTO> findById(@PathVariable String id){
        return new ResponseEntity<BookDTO>(booksService.findById(id), HttpStatus.FOUND);
    }

    @GetMapping
    public ResponseEntity<List<DetailedBookDTO>> findAllDetailedBooks(){
        return new ResponseEntity<List<DetailedBookDTO>>(booksService.findAllDetailedBook(),HttpStatus.OK);
    }
}
