package ru.semenov.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.semenov.dto.AuthorDTO;
import ru.semenov.entities.Author;
import ru.semenov.exceptions.ResourceNotFoundException;
import ru.semenov.repositories.AuthorsRepository;
import ru.semenov.services.IAuthorsService;

@Service
public class AuthorsServiceImpl implements IAuthorsService {
    private final AuthorsRepository authorsRepository;

    @Autowired
    public AuthorsServiceImpl(AuthorsRepository authorsRepository) {
        this.authorsRepository = authorsRepository;
    }

    @Transactional(readOnly = true)
    @Override
    public AuthorDTO findById(String id){
        Author author = authorsRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Автор не найден"));
        return new AuthorDTO(author);
    }
}
