package ru.semenov.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.semenov.dto.CategoryDTO;
import ru.semenov.entities.Category;
import ru.semenov.exceptions.ResourceNotFoundException;
import ru.semenov.repositories.CategoriesRepository;
import ru.semenov.services.ICategoryService;

import java.util.List;

@Service
public class CategoryServiceImpl implements ICategoryService {
    private final CategoriesRepository categoriesRepository;

    @Autowired
    public CategoryServiceImpl(CategoriesRepository categoriesRepository) {
        this.categoriesRepository = categoriesRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public CategoryDTO getCategoryById(String id) {
        Category categoryFound = categoriesRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("category not found"));
        return new CategoryDTO(categoryFound);
    }

    @Override
    @Transactional(readOnly = true)
    public List<CategoryDTO> getCategories(int pageNumber,int pageSize,String searchKey) {
        Pageable pageable = PageRequest.of(pageNumber,pageSize);
        List<Category> categories;
        if(searchKey.isEmpty()){
            categories=categoriesRepository.findAll();
        }else {
            categories = categoriesRepository.findByTitleContainingIgnoreCase(searchKey,pageable);
        }
        return categories.stream()
                .map(CategoryDTO::new)
                .toList();
    }
}
