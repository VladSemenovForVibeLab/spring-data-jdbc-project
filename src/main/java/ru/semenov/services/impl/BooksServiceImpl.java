package ru.semenov.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.semenov.dto.BookDTO;
import ru.semenov.dto.DetailedBookDTO;
import ru.semenov.entities.Book;
import ru.semenov.exceptions.ResourceNotFoundException;
import ru.semenov.repositories.BooksRepository;
import ru.semenov.services.IBooksService;

import java.util.List;

@Service
public class BooksServiceImpl implements IBooksService {
    private final BooksRepository booksRepository;

    @Autowired
    public BooksServiceImpl(BooksRepository booksRepository) {
        this.booksRepository = booksRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public BookDTO findById(String id) {
        Book book = booksRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Книга не найдена!"));
        return new BookDTO(book);
    }

    @Override
    public List<DetailedBookDTO> findAllDetailedBook() {
        return booksRepository.findDetailedBook();
    }
}
