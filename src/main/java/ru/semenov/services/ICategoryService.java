package ru.semenov.services;

import ru.semenov.dto.CategoryDTO;

import java.util.List;

public interface ICategoryService {
    CategoryDTO getCategoryById(String id);

    List<CategoryDTO> getCategories(int pageNumber, int pageSize,String searchKey);

}
