package ru.semenov.services;

import ru.semenov.dto.AuthorDTO;

public interface IAuthorsService {
    AuthorDTO findById(String id);
}
