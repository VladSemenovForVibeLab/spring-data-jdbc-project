package ru.semenov.services;

import ru.semenov.dto.BookDTO;
import ru.semenov.dto.DetailedBookDTO;

import java.util.List;

public interface IBooksService {
    BookDTO findById(String id);

    List<DetailedBookDTO> findAllDetailedBook();

}
