package ru.semenov.repositories;

import org.springframework.data.jdbc.repository.config.EnableJdbcRepositories;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.ListCrudRepository;
import org.springframework.stereotype.Repository;
import ru.semenov.dto.DetailedBookDTO;
import ru.semenov.entities.Book;

import java.util.List;

@EnableJdbcRepositories
@Repository
public interface BooksRepository extends ListCrudRepository<Book,String> {
   @Query("SELECT b.id, b.title, a.name, bd.description " +
           "FROM books b " +
           "LEFT JOIN authors a on b.author_id = a.id " +
           "LEFT JOIN books_details bd on bd.book_id = b.id")
    List<DetailedBookDTO> findDetailedBook();
}
