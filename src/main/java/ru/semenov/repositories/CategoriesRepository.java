package ru.semenov.repositories;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.ListCrudRepository;
import org.springframework.stereotype.Repository;
import ru.semenov.entities.Category;

import java.util.List;
import java.util.Optional;

@Repository
public interface CategoriesRepository extends ListCrudRepository<Category,String> {
    Optional<Category> findByTitle(String title);
    List<Category> findByTitleContainingIgnoreCase(String searchKey, Pageable pageable);
    @Query("SELECT * FROM categories WHERE title = :title")
    Optional<Category> nativeFindByTitle(String title);
    List<Category> findAllByIdGreaterThanAndIdLessThan(String minId,String maxId);
}
