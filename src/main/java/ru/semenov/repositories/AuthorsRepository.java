package ru.semenov.repositories;

import org.springframework.data.repository.ListCrudRepository;
import org.springframework.stereotype.Repository;
import ru.semenov.entities.Author;

@Repository
public interface AuthorsRepository extends ListCrudRepository<Author,String> {
}
