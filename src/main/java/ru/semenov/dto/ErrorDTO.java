package ru.semenov.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@ToString
@EqualsAndHashCode
@Setter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ErrorDTO {
    private String message;
    private LocalDateTime dateTime;
    public ErrorDTO(String message){
        this.message = message;
        this.dateTime = LocalDateTime.now();
    }
}
