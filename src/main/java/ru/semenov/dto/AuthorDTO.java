package ru.semenov.dto;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.MappedCollection;
import ru.semenov.entities.Author;
import ru.semenov.entities.Book;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class AuthorDTO {
    @Id
    private String id;
    private String name;
    @MappedCollection(idColumn = "author_id")
    private Set<Book> books;

    public AuthorDTO(Author author){
        this.id =author.getId();
        this.name =author.getName();
        this.books=author.getBooks();
    }
}
