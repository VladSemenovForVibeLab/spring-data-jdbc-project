package ru.semenov.dto;

import lombok.*;
import ru.semenov.entities.Book;
import ru.semenov.entities.BookDetails;

@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@ToString
@EqualsAndHashCode
public class BookDTO {
    private String id;
    private String title;
    private BookDetails bookDetails;
    public BookDTO(Book book){
        this.id = book.getId();
        this.title = book.getTitle();
        this.bookDetails = book.getBookDetails();
    }
}
