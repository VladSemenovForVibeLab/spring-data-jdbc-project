package ru.semenov.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@ToString
@EqualsAndHashCode
public class DetailedBookDTO {
    private String id;
    private String title;
    private String authorName;
    private String description;
}
