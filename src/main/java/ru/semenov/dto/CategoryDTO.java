package ru.semenov.dto;

import lombok.*;
import org.springframework.data.annotation.PersistenceCreator;
import ru.semenov.entities.Category;

@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class CategoryDTO {
    private String id;
    private String title;
    @PersistenceCreator
    public CategoryDTO(Category category){
        this.id = category.getId();
        this.title = category.getTitle();
    }
}
